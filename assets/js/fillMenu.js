"use strict"

function image(name,alt){
    this.name=name;
    this.alt=alt;
};

function action(name,img,link){
    this.name=name;
    this.img=img;
    this.link=link;
};

let actions = new Array();

let news = new image("assets/images/News.svg","News");
let box = new image("assets/images/Box.svg", "Box");
let home = new image("assets/images/Classroom.svg","Classroom");
let contacts = new image("assets/images/Contacts.svg","Contacts");
let services = new image("assets/images/Services.svg","Services");
let calendar = new image("assets/images/Calendar.svg","Calendar");

actions.push(new action("Inventura",news,"#inventura"));
actions.push(new action("Inventar", box, "#inventar"));
actions.push(new action("Prostorije", home, "#prostorije"));
actions.push(new action("Zaposlenici", contacts, "#zaposlenici"));
actions.push(new action("Administracija", services, "#administracija"));

function fillFstNavi() {
    const parent = document.getElementById("fstList");
    for(let k in actions) {
        const x = document.createElement("li");
        parent.appendChild(x);
        parent.childNodes[2*k].setAttribute("class", "fstNaviListEle");
        const x1 = document.createElement("li");
        parent.appendChild(x1);
        parent.childNodes[2*k+1].setAttribute("class", "bottomBorder");
        const y = document.createElement("a");
        parent.childNodes[2*k].appendChild(y);
        parent.childNodes[2*k].firstChild.setAttribute("href",actions[k].link);
        const z = document.createElement("img");
        parent.childNodes[2*k].firstChild.appendChild(z);
        parent.childNodes[2*k].firstChild.firstChild.setAttribute("src",
        actions[k].img.name);
        parent.childNodes[2*k].firstChild.firstChild.setAttribute("alt",
        actions[k].img.alt);
        const w = document.createElement("h3");
        parent.childNodes[2*k].firstChild.appendChild(w);
        parent.childNodes[2*k].firstChild.lastChild.innerHTML = actions[k].name;
    }
};