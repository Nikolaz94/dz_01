"use strict"

function room(name,itemsCount){
    this.name=name;
    this.itemsCount=itemsCount;
};

let rooms = [];

rooms.push(new room("Predavaonica 1",50));
rooms.push(new room("Predavaonica 2",50));
rooms.push(new room("Predavaonica 3",50));
rooms.push(new room("Predavaonica 4",50));
rooms.push(new room("Predavaonica 5",50));
rooms.push(new room("Predavaonica 6",50));
rooms.push(new room("Portirnica",50));
rooms.push(new room("Referada",50));

function fillScndNavi(){
    const parent = document.getElementById("scndList");
    for(let k in rooms) {
        const x = document.createElement("li");
        parent.appendChild(x);
        parent.childNodes[k].setAttribute("class", "scndNaviListEle");
        const y = document.createElement("h3");
        parent.childNodes[k].appendChild(y);
        parent.childNodes[k].firstChild.innerHTML = rooms[k].name;
        const z = document.createElement("p");
        parent.childNodes[k].appendChild(z);
        parent.childNodes[k].lastChild.innerHTML = `Broj predmeta: 
        ${rooms[k].itemsCount}`;
    }
};

function sort(parameter) {
    const alt1 = "Arrow";
    const alt2 = "UpArrow";
    //console.log(parameter == alt2 ? 1 : -1);
    const parent = document.getElementById("scndList");
    const scrollBtn = document.getElementById("scroll");
    if(parameter === alt1) {
        scrollBtn.childNodes[1].childNodes[1].src = "assets/images/UPArrow.svg";
        scrollBtn.childNodes[1].childNodes[1].alt = alt2;
        rooms.sort(function(a, b) {return a.name < b.name ? 1 : -1});
    }
    if(parameter === alt2) {
        scrollBtn.childNodes[1].childNodes[1].src = "assets/images/Arrow.svg";
        scrollBtn.childNodes[1].childNodes[1].alt = alt1;
        rooms.sort(function(a, b) {return a.name > b.name ? 1 : -1});
    }
    for(let k in rooms) {
        parent.childNodes[k].firstChild.innerHTML = rooms[k].name;
        parent.childNodes[k].lastChild.innerHTML = `Broj predmeta: 
        ${rooms[k].itemsCount}`;
    }
};